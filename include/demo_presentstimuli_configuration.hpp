//
// Copyright (c) 2017-2020 Santini Designs. All rights reserved.
//

#pragma once

#include <basic/types.hpp>

#include <eye/protocol.hpp>

namespace user_tasks::demo_presentstimuli {

/**
 * The welcome banner configuration class is configuration class for the welcome banner task.
 */
struct Demo_presentStimuliConfiguration : public eye::protocol::EyerisTaskConfiguration
{
    using ptr_t = std::shared_ptr<Demo_presentStimuliConfiguration>;  ///< Pointer type definition for the class

    /**
     * @brief Static factory.
     *
     * @param[in] other Pointer to another JSON object that defines the initial schema of this one.
     *
     * @return Pointer to a new class instance
     */
    [[maybe_unused]]
    static ptr_t factory_ptr(const basic::types::JSON::sptr_t& other)
    {
        return std::make_shared<Demo_presentStimuliConfiguration>(other);
    }

    /**
     * @brief Constructor that instantiate the configuration from the prototype, but then let set the values
     * also based on the parent configuration.
     *
     * @param[in] other Pointer to a JsonObject used to initialize the configuration
     */
    explicit Demo_presentStimuliConfiguration(basic::types::JSON::sptr_t other) :
        EyerisTaskConfiguration(other)
    {
        // This is where you set the value to your configuration variable
        initializerecalSquareSize(5);// this value can get overwritten from insight
        initializefixMarkerSize(8);
        initializefixMarkerWidth(2);
        initializeRecalFlag(false);
    }
    // This is where you will create your configuration variable
    // NOTE: LC_PROPERTY_ ->  after the under score can be casted as any variable type. In this example we have an INT
    // but it could be LC_PROPERTY_FLOAT or LC_PROPERTY_BOOL;
    //LC_PROPERTY_DATATYPE( has three parameters)
    // 1. Variable name for configuration
    // 2."the string that shows up in insight"
    // 3. the variable name that you assign the value to -> this must be the same as where your are assigning the variable
    LC_PROPERTY_INT(RECALSQUARESIZE, "1_Recalibration Square Size", recalSquareSize);
    LC_PROPERTY_INT(FIXMARKERSIZE, "2_Fixation marker Size", fixMarkerSize);
    LC_PROPERTY_INT(FIXMARKERWIDTH,"3_Fixation marker width",fixMarkerWidth);
    LC_PROPERTY_BOOL(RECALFLAG,"4_Recalibration Flag",RecalFlag);
};

}  // namespace user_tasks::demo_presentstimuli

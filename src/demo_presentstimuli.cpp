#include "demo_presentstimuli.hpp"

#include <eye/configuration.hpp>
#include <eye/messaging.hpp>

namespace user_tasks::demo_presentstimuli {

EYERIS_PLUGIN(Demo_presentStimuli)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Public methods

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Demo_presentStimuli::Demo_presentStimuli() :
    EyerisTask()
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Demo_presentStimuli::eventCommand(int command, const basic::types::JSONView& arguments)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Demo_presentStimuli::eventConfiguration(const Demo_presentStimuliConfiguration::ptr_t& configuration)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Demo_presentStimuli::eventConsoleChange(const basic::types::JSONView& change)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Demo_presentStimuli::finalize()
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Demo_presentStimuli::initialize()
{
    // Setup parameters
    // The pixel angle defines the conversion between the number of pixels on the display to the physical size of the
    // object in minutes of arc
    pixelAngle = getAngleConverter()->pixel2ArcminH(1);
    // Where you create your stimuli and set values to variables
    // Eyeris will only go into this function once per Block, so only define variables here that stay the same for an
    // entire block

    ///Create your stimuli
    //Create a fixation square
    // 1. Choose the class of object that you need
    // 2. Create a new object
    // 3. Put the variable name in the .hpp file

    FixMarkerSize=getConfiguration()->getfixMarkerSize()/ pixelAngle;
    FixMarkerWidth=getConfiguration()->getfixMarkerWidth()/pixelAngle;
    // We create the fixation cross here using the sizes provided in the configuration file
    // Having the parameters specified in the configuration file also allows the user
    // to make changes from insight so they do not have to rebuild the experiment each time
    m_fixCross= newCrossPlane(FixMarkerSize,FixMarkerWidth, eye::graphics::RGB(0,0,0));
    m_fixCross->setPosition(0,0);
    m_fixCross->hide();

    // The following objects would used in the recalibration phases where subjects, where one of the boxes would be
    // centered on the display and the other gaze contingent with the subject's eye motion
    // To ensure precise tracking we would have the recalibration done at the end/beginning of each trial to account for
    // small heaad motion that might happen between trials
    //create a white square
    recalSquareSize=getConfiguration()->getrecalSquareSize()/pixelAngle;
    recalSquare_white = newSolidPlane(recalSquareSize, recalSquareSize,eye::graphics::RGB(255, 255, 255)); // everything is in pixels
    recalSquare_white->setPosition(0, 0); // in pixels
    recalSquare_white->hide();

    recalSquare_black = newSolidPlane(recalSquareSize, recalSquareSize,eye::graphics::RGB(0, 0, 0)); // everything is in pixels
    recalSquare_black->setPosition(0, 0); // in pixels
    recalSquare_black->hide();

    ///Create a black square with specification for your configuration
    // 1. initialize the variable in the configuration.hpp
    // 2. initialize the variable name the .hpp file
    // 3. Read the value from your configuration file in the .cpp file and set it to the  variable fixSquareSize

    /// Create your states... this is done in the hpp file

    /// Declare the duration for your timers

    fixationDuration = 2000ms; // you need to put ms (milliseconds) for the timer class
    blankDuration=3000ms; 
    // We would always want to start the experiment from the loading state so we set that here so that when we
    // enter the Demo_presentStimuli::streamEye we would start at the loading state
    // Note: For simplicity we currently have only two states in this experiment namely STATE_RECALIBRATION and
    // STATE_FIXATION and the experiment simply toggles between the two on each trial
    
    subjectSubmittedCalibration=0;// We initially set this to false/0 so that it waits for the subject to make a key press
                                  // before moving on
    RecalFlag=getConfiguration()->getRecalFlag();// Setting this true sets up a recalibration state
    if (RecalFlag==true){
        X_eye = 0; // location of gaze in pixels
        Y_eye = 0;
        x_shift=0;
        y_shift=0;
        change_state_to=STATE_RECALIBRATION;// We start with the recalibration state
    }else{
        change_state_to=STATE_FIXATION;//otherwise we go back and forth between the blank and fixation states
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Demo_presentStimuli::setup()
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Demo_presentStimuli::streamAnalog(const eye::signal::DataSliceAnalogBlock::ptr_t& data)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Demo_presentStimuli::streamDigital(const eye::signal::DataSliceDigitalBlock::ptr_t& data)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Demo_presentStimuli::streamEye(const eye::signal::DataSliceEyeBlock::ptr_t& data)
{
    ///This is where the main experiment happens
    //For first time coders: When eyeris is in this function, it samples anything out of a switch case statement every
    // monitor (video card) sample.
    if (RecalFlag==true){
        // We obtain eye positions
        auto slice = data->getLatest();
        // Apply recalibration offsets
        X_eye = getAngleConverter()->arcmin2PixelH(slice->calibrated1.x())+xshift;
        Y_eye = getAngleConverter()->arcmin2PixelV(slice->calibrated1.y())+yshift;
    }

    switch (change_state_to) {
        // Since change_state_to was set to STATE_RECALIBRATION in initialze this where the first iteration/trial starts
        case STATE_RECALIBRATION:
            if (subjectSubmittedCalibration != 1) { /// continue until subject makes a keypress to confirm recal (press x)
                ///show the recalibration sqaures
                recalSquare_white->show();
                recalSquare_black->setPosition(xPos,yPos);
                recalSquare_black->show();
            }else{
                recalSquare_white->hide();
                recalSquare_black->hide();
                // We save the offset provided by the subject
                xshift = xPos + xshift;
                yshift = yPos + yshift;
                /// Prep the next stimuli
                //start the timer for however long you want to show it
                fixationTimer.start(fixationDuration);

                /// Change the state: tell it where you want to go next
                //for example: after recalibration you want to show a fixation square, so you go to state fixation
                change_state_to = STATE_FIXATION;
                info("\n Recalibration finalized!");// These messages would show up in the system log on insight
                                                    // generally handy for debugging and getting real-time information
                                                    // during the experiment (for example to monitor performance)
            }
            break;
        case STATE_FIXATION:
            if (!fixationTimer.hasExpired()){ /// continue until the timer for 1000ms has expired
                m_fixCross->show();// we show the fixation cross during this interval
            }else{
                m_fixCross->hide(); //It is good practice to hide stimuli away at the end of each state so they don't
                                    // remain on the display forever!
                info("\n Fixation interval ended!");
                /// End of trial
                // Reset parameters before the start of the next trial
                if (RecalFlag==true){
                    change_state_to=STATE_RECALIBRATION;// Here it would go back to first state of the experiment
                    subjectSubmittedCalibration=0;
                }else{
                    change_state_to=STATE_BLANK;
                    //start the timer for the blank interval
                    blankTimer.start(blankDuration);
                }
            }
           break;
        case STATE_BLANK:
            if (!blankTimer.hasExpired()){ //added this here for consistency
                //Do nothing
            }else{
                //Go back to fixation state at the end of timer
                change_state_to=STATE_FIXATION;
                //Start fixation timer
                fixationTimer.start(fixationDuration);
            }
            break;
        }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void Demo_presentStimuli::streamKeyboard(const eye::signal::DataSliceKeyboardBlock::ptr_t& data) {
        auto keyboard = data->getLatest();
        switch (change_state_to) {
            // When we are in the recalibration state we would like the get the key presses from the subject to move
            // one of the recalibration boxes
            // Here we use WASD to move the box up, left, down, right (respectively)
            // Once ready subjects would press 'X' to confirm their response
            case STATE_RECALIBRATION: {
                if ((keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_w)) |
                    (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_W)))  // moving the cursor up
                {
                    yPos = yPos + Increment; //position of the cross
                } else if ((keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_z)) |
                           (keyboard->isKeyReleased(
                                   source_keyboard::keyboard_keys_e::KEY_Z)))  // moving the cursor down
                {
                    yPos = yPos - Increment;
                } else if ((keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_d)) |
                           (keyboard->isKeyReleased(
                                   source_keyboard::keyboard_keys_e::KEY_D)))// moving the cursor to the right
                {
                    xPos = xPos + Increment;

                } else if ((keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_a)) |
                           (keyboard->isKeyReleased(
                                   source_keyboard::keyboard_keys_e::KEY_A))) // moving the cursor to the left
                {
                    xPos = xPos - Increment;
                } else if ((keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_s)) |
                           (keyboard->isKeyReleased(
                                   source_keyboard::keyboard_keys_e::KEY_S))) // moving the cursor to the left
                {
                    yPos = yPos - Increment;
                }
                if ((keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_x)) |
                    (keyboard->isKeyReleased(source_keyboard::keyboard_keys_e::KEY_X)))// finalize the response
                {
                    info("Recalibration finalized");
                    subjectSubmittedCalibration = 1;
                }
            }
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Demo_presentStimuli::streamJoypad(const eye::signal::DataSliceJoypadBlock::ptr_t& data)
{
    auto joypad= data->getLatest();
    switch (change_state_to) {
        case STATE_RECALIBRATION: {
            if (joypad->isButtonPressed(
                    source_joypad::joypad_buttons_e::BUTTON_UP)) {
                if (Badal==true){
                    yPos = yPos - Increment; //position of the cross
                }else{
                    yPos = yPos + Increment; //position of the cross
                }
            } else if (joypad->isButtonPressed(
                    source_joypad::joypad_buttons_e::BUTTON_DOWN)) // moving the cursor down
            {
                if (Badal==true){
                    yPos = yPos + Increment; //position of the cross
                }else{
                    yPos = yPos - Increment; //position of the cross
                }
            } else if (joypad->isButtonPressed(
                    source_joypad::joypad_buttons_e::BUTTON_RIGHT)) // moving the cursor to the right
            {
                if (Mirror==true && Badal==false) {
                    xPos = xPos - Increment;
                }else if (Mirror==true && Badal==true){
                    xPos = xPos + Increment;
                }
            } else if (joypad->isButtonPressed(
                    source_joypad::joypad_buttons_e::BUTTON_LEFT)) // moving the cursor to the left
            {
                if (Mirror==true && Badal==false){
                    xPos = xPos + Increment;
                }else if (Mirror==true && Badal==true){
                    xPos = xPos - Increment;
                }
            }
            if (joypad->isButtonPressed(source_joypad::joypad_buttons_e::BUTTON_TRIANGLE))// finalize the response
            {
                info("Recalibration finalized");
                ResponseFinalize = 1;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Demo_presentStimuli::streamMonitor(const eye::signal::DataSliceMonitorBlock::ptr_t& data)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Demo_presentStimuli::streamMouse(const eye::signal::DataSliceMouseBlock::ptr_t& data)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Demo_presentStimuli::streamVideoCard(const eye::signal::DataSliceVideoCardBlock::ptr_t& data)
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Demo_presentStimuli::teardown()
{
    // Nothing to do
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Protected methods

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Private methods

}  // namespace user_tasks::demo_presentstimuli
